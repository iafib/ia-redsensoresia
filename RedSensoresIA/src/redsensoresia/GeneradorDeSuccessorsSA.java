package redsensoresia;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import redsensoresia.estat.Centre;
import redsensoresia.estat.Connectable;
import redsensoresia.estat.Estat;
import redsensoresia.estat.Sensor;
import aima.search.framework.Successor;
import aima.search.framework.SuccessorFunction;

public class GeneradorDeSuccessorsSA implements SuccessorFunction {

	@Override
	public List<Successor> getSuccessors(Object arg0) {
		
		Estat actual = (Estat) arg0; //Estat actual
		ArrayList<Successor> possiblesSuccessors = new ArrayList<Successor>();
		Estat nou = new Estat(actual);
		String operador;
		
		Random myRandom = new Random(1);
		
		int op = myRandom.nextInt(2);
		int i,j;
		char c;
		
		switch(op) {
		
		/* Operador Afegir */
		case 0:
			i = myRandom.nextInt(actual.sensors.size());
			
			int t = myRandom.nextInt(1);
			if(t == 1) {
				j = myRandom.nextInt(actual.sensors.size());
				c = 's';
			}
			else {
				j = myRandom.nextInt(actual.centres.size());
				c = 'c';
			}
			while(!nou.afegir(i,j,c)) {
				t = myRandom.nextInt(1);
				if(t == 1) {
					j = myRandom.nextInt(actual.sensors.size());
					c = 's';
				}
				else {
					j = myRandom.nextInt(actual.centres.size());
					c = 'c';
				}
			}
			possiblesSuccessors.add(new Successor("afegir",nou));
			break;
		
		/* Operador Intercanviar */				
		case 1:
			i = myRandom.nextInt(actual.sensors.size());
			j = myRandom.nextInt(actual.sensors.size());
			c = 's';
			while(!nou.intercanvi(i, j, c)) {
				i = myRandom.nextInt(actual.sensors.size());
				j = myRandom.nextInt(actual.sensors.size());
			}
			possiblesSuccessors.add(new Successor("intercanviar",nou));
			break;
		
		/* Operador Eliminar */	
		case 2:
			i = myRandom.nextInt(actual.sensors.size());
			while(!nou.eliminar(i)) {
				i = myRandom.nextInt(actual.sensors.size());
			}
			possiblesSuccessors.add(new Successor("eliminar",nou));
			break;
		}
	
		return possiblesSuccessors;
	}

}
