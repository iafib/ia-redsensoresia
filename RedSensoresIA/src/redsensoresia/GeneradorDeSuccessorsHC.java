package redsensoresia;
import java.util.ArrayList;
import java.util.List;

import redsensoresia.estat.Estat;
import aima.search.framework.Successor;
import aima.search.framework.SuccessorFunction;


public class GeneradorDeSuccessorsHC implements SuccessorFunction {

	@Override
	public List<Successor> getSuccessors(Object arg0) {		
		Estat actual = (Estat) arg0; //Estat actual
		ArrayList<Successor> possiblesSuccessors = new ArrayList<Successor>();
		
		/* Operador Afegir */
		
		for(int i = 0; i < actual.sensors.size(); ++i) {
			for(int j = 0; j < actual.sensors.size(); ++j) {
				Estat nou = new Estat(actual);
				if(nou.afegir(i,j,'s')) possiblesSuccessors.add(new Successor("afegir",nou));
			}
			
			for(int j = 0; j < actual.centres.size(); ++j) {
				Estat nou = new Estat(actual);
				if(nou.afegir(i,j,'c')) possiblesSuccessors.add(new Successor("afegir",nou)); 
			}
		}
		
		
		//Operador Intercanviar 
		
		for(int i = 0; i < actual.sensors.size(); ++i) {
			for(int j = 0; j < actual.sensors.size(); ++j) {
				Estat nou = new Estat(actual);
				if(nou.intercanvi(i,j,'s')) possiblesSuccessors.add(new Successor("intercanviar", nou));
			}
		}
		
		/* Operador Eliminar */
		
		for(int i = 0; i < actual.sensors.size(); ++i) {
			Estat nou = new Estat(actual);
			nou.eliminar(i);
			possiblesSuccessors.add(new Successor("eliminar", nou));
		}
		
		return possiblesSuccessors;
	}

}
