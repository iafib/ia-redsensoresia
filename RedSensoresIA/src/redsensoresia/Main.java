package redsensoresia;
import aima.search.framework.Problem;
import aima.search.framework.Search;
import aima.search.framework.SearchAgent;
import aima.search.informed.HillClimbingSearch;
import aima.search.informed.SimulatedAnnealingSearch;
import redsensoresia.estat.Estat;

public class Main {

	public static void main(String[] args) throws Exception {
		int llavor = 1;
		int nombreDeSensors = 100;
		int nombreDeCentres = 3;
		
		Estat e = new Estat(Estat.SOLUCIO_PRIMER_SENSORS_5_3_1, llavor, nombreDeSensors, nombreDeCentres);
		
		System.out.println("Valor dades obtingudes: "+e.getDadesObtingudes()+"MB");
		System.out.println("Cost infreaestructura: "+e.getCostInfraestructura() );
		
		Heuristic h = new Heuristic();
		System.out.println("Heuristic Solucio inicial: "+h.getHeuristicValue(e) );
		
		System.out.println("Olaaa k ase");
		
		Problem redSensores = new Problem(e, new GeneradorDeSuccessorsHC(), new GoalTest(), new Heuristic()  );
		Search hCS = new HillClimbingSearch();
		SearchAgent sA = new SearchAgent(redSensores, hCS);
		
		e = (Estat) hCS.getGoalState();
		
		System.out.println(h.getHeuristicValue(e));
	}

}
