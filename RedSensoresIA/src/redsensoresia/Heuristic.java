package redsensoresia;
import redsensoresia.estat.Estat;
import aima.search.framework.HeuristicFunction;


public class Heuristic implements HeuristicFunction {

	@Override
	public double getHeuristicValue(Object arg0) {
		
		Estat e = (Estat) arg0;
		
		double totalTrafic = e.getDadesObtingudes();
		
		double costInfraestructura = e.getCostInfraestructura();
		
		System.out.println(-totalTrafic*5000000 + costInfraestructura);
		
		return -totalTrafic*5000000 + costInfraestructura;
	}

}
