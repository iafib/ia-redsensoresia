package redsensoresia.estat;

public interface Connectable {
	
	boolean estaConnectat();
	
	boolean pucConnectarme();
	
	void incrementaConnexionsOcupades();
	
	boolean superaTransitMaxim(int volum);

	void incrementaTransitActual(int volum);

	int getCoordX();

	int getCoordY();

	void decrementaConnexionsOcupades();

	void decrementaTransitActual(int volum);

}
