package redsensoresia.estat;

import IA.Red.Centro;

public class Centre extends Centro implements Connectable{

	private static final int CONNEXIONS_MAXIMES = 25;
	private static final int TRANSIT_MAXIM = 200;
	
	private int connexionsOcupades;
	private int transitActual;

	public Centre(Centro cent) {
		super(cent.getCoordX(), cent.getCoordY() );
		setConnexionsOcupades(0);
		transitActual = 0;
	}
	
	public Centre(Centre cen) {
		super(cen.getCoordX(), cen.getCoordY() );
		connexionsOcupades = cen.connexionsOcupades;
		transitActual = cen.transitActual;
	}
	
	public int getConnexionsOcupades() {
		return connexionsOcupades;
	}

	public void setConnexionsOcupades(int connexionsOcupades) {
		this.connexionsOcupades = connexionsOcupades;
	}

	@Override
	public boolean estaConnectat() {
		return true;
	}

	@Override
	public boolean pucConnectarme() {
		return connexionsOcupades + 1 <= CONNEXIONS_MAXIMES;
	}

	@Override
	public void incrementaConnexionsOcupades() {
		setConnexionsOcupades(getConnexionsOcupades() + 1);
		
	}

	@Override
	public boolean superaTransitMaxim(int volum) {
		return  transitActual + volum > TRANSIT_MAXIM;
	}

	@Override
	public void incrementaTransitActual(int volum) {
		transitActual += volum;
	}

	public double getTransitActual() {
		return transitActual;	
	}

	@Override
	public void decrementaConnexionsOcupades() {
		--connexionsOcupades;
	}

	@Override
	public void decrementaTransitActual(int volum) {
		transitActual -= volum;
	}
}
