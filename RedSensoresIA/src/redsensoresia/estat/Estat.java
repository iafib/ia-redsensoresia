package redsensoresia.estat;
import java.util.ArrayList;
import java.util.Collections;

import IA.Red.CentrosDatos;
import IA.Red.Sensores;

public class Estat {

	public static final int SOLUCIO_SENSORS_A_CENTRE = 1;
	public static final int SOLUCIO_PRIMER_SENSORS_5_3_1 = 2;
	public ArrayList<Sensor> sensors;
    public ArrayList<Centre> centres;
    private int traficTotal;
    
    public ArrayList<Object[]> hiHaCicle; 
    
    public Estat(int solIni, int llavor, int nombreDeSensors, int nombreDeCentres) {
    	
    	Sensores s = new Sensores(nombreDeSensors, llavor);
		CentrosDatos c = new CentrosDatos(nombreDeCentres, llavor);
		
		hiHaCicle = new ArrayList<Object[]>(); 
				
		switch(solIni) {
			case SOLUCIO_SENSORS_A_CENTRE:
				solucioInicial1(s, c);
				break;
			case SOLUCIO_PRIMER_SENSORS_5_3_1:
				solucioInicial2(s, c);
				break;
		}
		
		traficTotal = 0;
		
		for (Sensor sen: sensors) {
			traficTotal += sen.getCapacidad();
		}	
	}
    
    public Estat(Estat actual) {
    	sensors = new ArrayList<Sensor>(actual.sensors.size() );
    	centres = new ArrayList<Centre>(actual.centres.size() );
		traficTotal = actual.traficTotal;
		for (Sensor sen : actual.sensors) {
			sensors.add(new Sensor(sen) );
		}
		for (Centre cen : actual.centres) {
			centres.add(new Centre(cen) );
		}
	}

	private void solucioInicial1(Sensores s, CentrosDatos c) {
		
		sensors = new ArrayList<Sensor>(s.size() );
    	centres = new ArrayList<Centre>(c.size() );
    	
		for (IA.Red.Sensor sens : s) {
			sensors.add(new Sensor(sens) );
		}
		
		/*******cout***********
		int sensors1 = 0;
		int sensors2 = 0;
		int sensors5 = 0;
		for(int i = 0; i < sensors.size(); ++i) {
			Sensor a = sensors.get(i);
			if(a.getCapacidad()==1) ++sensors1;
			if(a.getCapacidad()==2) ++sensors2;
			if(a.getCapacidad()==5) ++sensors5;
		}
		System.err.println("sensors.size: "+sensors.size());
		System.err.println("sensors1: "+sensors1);
		System.err.println("sensors3: "+sensors2);
		System.err.println("sensors5: "+sensors5);
		/***********************/
		
		for (IA.Red.Centro cent : c) {
			centres.add(new Centre(cent) );
		}
		
		
		int j = 0;
		boolean assignat = false;
		
		for(int i = 0; i < sensors.size() && j < centres.size(); ++i ) {
			//System.err.println("**********ITERACIO del FOR: "+i);			
			assignat = false;

			while(!assignat && j < centres.size()) {
				assignat = connecta(i, j, 'c');
				if(!assignat){
					++j;
					//System.err.println("j: "+j);
				}
				//else System.err.println("sensor assignat!\n");
			}
		}
	}
	
    private void solucioInicial2(Sensores s, CentrosDatos c) {
		/*En aquesta solucio inicial s'intenta assignar els sensors, ordenats decreixentment segons 
		 * la capacitat ( +captura => +cap. transmissio) als centres de dades, mentre sigui possible.
		 * En cas que no sigui possible es prova d'assignar als sensors a sensors que ja estan enllasats i 
		 *  vigiliant que no es connectin a ells mateixos.
		 */
		
		sensors = new ArrayList<Sensor>(s.size() );
		centres = new ArrayList<Centre>(c.size() );
		
		for (IA.Red.Sensor sens : s) {
			sensors.add(new Sensor(sens) );
		}

		for (IA.Red.Centro cent : c) {
			centres.add(new Centre(cent) );
		}
		//System.err.println("centres.size: "+centres.size());
		
		int j = 0;
		Collections.sort(sensors);	//ordena els sensors decreixentment 5,3,1
		for(int i = 0; i < sensors.size(); ++i) {
			//System.err.println("**********ITERACIO del FOR: "+i);
			Sensor sen = sensors.get(i);
			boolean trobat = false;
			while(!trobat && j < centres.size() ) {
				trobat = connecta(i,j, 'c');
				if (!trobat) {
					//System.err.println("j: "+j);
					++j;
				}
			}
			System.err.println("fi_while");
			for (int k = 0; k < sensors.size() && !trobat; ++k) { //si no conecta a centre ho prova a un sensor
				Sensor sen1 = sensors.get(k);
				if (!sen1.equals(sen)) {
					trobat = connecta(i, k, 's');		
				}
				System.err.println("fi_for");
			}
		}
    }
		
	
    //i = sensor a connectar, j = connectable, tipus = tipus del connectable
	private boolean connecta(int i, int j, char tipusConnectable) {
		Sensor sen = sensors.get(i);
		
		hiHaCicle = new ArrayList<Object[]>(); 
		
		if (!sen.estaConnectat() && ocuparConnexio( (int)sen.getTransitActual(), j, tipusConnectable)) {
			sen.setConnexioSortint(j);
			sen.setC(tipusConnectable);
			hiHaCicle.add(new Object[] { i, 's'});
			
			for (int a = 0; a < hiHaCicle.size(); ++a) {
				for (int b = 0; b < hiHaCicle.size(); ++b) {
					if (b != a
							&& hiHaCicle.get(a)[0] == hiHaCicle.get(b)[0]
							&& hiHaCicle.get(a)[1] == hiHaCicle.get(b)[1])
					{
						System.out.println("Hi ha cicle! Entre: "
								+ hiHaCicle.get(a)[0] + " "
								+ hiHaCicle.get(a)[1] + " "
								+ hiHaCicle.get(b)[0] + " "
								+ hiHaCicle.get(b)[1]);
					}
				}
			}
			
			return true;
		}
		return false;
	}
	
	//volum = volum que ve del connectat, j = connectable, tipus = tipus del connectable
	private boolean ocuparConnexio(int volum, int j, char tipusConnectable) {
		Connectable con;
		if (tipusConnectable == 'c') con = centres.get(j);
		else con = sensors.get(j);
		
		if (con.estaConnectat() && con.pucConnectarme() && propaga(j, tipusConnectable, volum) ) {
			con.incrementaConnexionsOcupades();
			return true;
		}
		return false;
	}
	
	//j = node actual, tipus = som centre o sensor, volum = dades que treu el node qe volem connectar
	private boolean propaga(int j, char tipus, int volum) {
		Connectable con;
		
		boolean ok = true;
		if(tipus == 'c') {
			con = centres.get(j);
		}
		else {
			con = sensors.get(j);
		}
		//sempre cal mirar que no es superi el transit max.
		ok = !con.superaTransitMaxim(volum);	
		//si sensor, cal fer recursivitat...
		if(tipus == 's') {
			Sensor s = (Sensor) con;
			ok = ok && propaga(s.getConnexioSortint(), s.getC(), volum);
		}
		
		if (ok) {
			con.incrementaTransitActual(volum);
			hiHaCicle.add(new Object[] { j, tipus});
		}
		return ok;
	}

	public double getDadesObtingudes() {
		
		double dadesObtingudes = 0;
		
		for (Centre c: centres) {
			dadesObtingudes += c.getTransitActual();
		}
		
		return dadesObtingudes;
	}

	public double getCostInfraestructura() {
		
		double costInfraestructura = 0;
		
		for (Sensor s: sensors) {
			costInfraestructura += costEnllas(s);
		}
		
		return costInfraestructura;
	}

	private double costEnllas(Sensor s) {
		if (s.getC() == 'n') return 0;
		
		Connectable con;
		if (s.getC() == 'c') {
			con = centres.get(s.getConnexioSortint() );
		}
		else {
			con = sensors.get(s.getConnexioSortint() );
		}
		
		return Math.pow(s.getTransitActual(), 2)*Math.pow(distanciaEuclidea(s, con), 2);
	}

	private double distanciaEuclidea(Sensor s, Connectable con) {
		return Math.sqrt(Math.pow(s.getCoordX() - con.getCoordX(), 2) + Math.pow(s.getCoordY() - con.getCoordY(), 2));
	}

	public double getTraficTotal() {
		return traficTotal;
	}
	
	public boolean afegir (int i, int j, char c) {
		return connecta(i,j,c);
	}

	public boolean intercanvi(int i, int j, char c) {
		if(sensors.get(i).getC() != 'n' && sensors.get(j).getC() != 'n') {
			int desti1 = sensors.get(i).getConnexioSortint();
			int desti2 = sensors.get(j).getConnexioSortint();
			char c1;
			char c2;
			
			c1 = sensors.get(i).getC();
			c2 = sensors.get(j).getC();
			
			eliminar(i);
			eliminar(j);
			if (connecta(i,desti2,c2) && connecta(j,desti1,c1)) return true;
			else {
				connecta(i,desti1,c1);
				connecta(j,desti2,c2);
				return false;
			}
		}
		return false;
	}

	//i = index del sensor a desconnectar
	public boolean eliminar(int i) {
		Sensor s = sensors.get(i);
		if(s.estaConnectat()) {
			desocupaConnexio(s.ConnexioSortint, s.c, s.transitActual);
			s.ConnexioSortint = 0;
			s.c = 'n';
			return true;
		}
		else return false;
	}

	//i = index del connectable del que es desconnecta, c = tipus de desconnectable, volum, quantitat de dades que deixen de passar-hi
	private void desocupaConnexio(int i, char c, int volum) {
		Connectable con;
		if (c == 's') {
			con = sensors.get(i);
		}
		else {
			con = centres.get(i);
		}
		propagaDesconnexio(i, c, volum);
		con.decrementaConnexionsOcupades();
		
	}

	//i = index del node del que reduirem volum, c = tipus del node, volum = volum a reduir
	private void propagaDesconnexio(int i, char c, int volum) {
		Connectable con;
		if (c == 's') {
			con = sensors.get(i);
			Sensor sen = (Sensor) con;
			propagaDesconnexio(sen.getConnexioSortint(), sen.getC(), volum);
		}
		else {
			con = centres.get(i);
		}
		con.decrementaTransitActual(volum);
	}
	
}
