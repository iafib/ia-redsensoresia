package redsensoresia.estat;

public class Sensor extends IA.Red.Sensor implements Comparable<Sensor>, Connectable{
	
	private static final int CONNEXIONS_MAXIMES = 3;

	public int connexionsOcupades;
	public int transitActual;
	public int ConnexioSortint;
	public char c;
	
	public Sensor(IA.Red.Sensor sens) {
		super( (int) sens.getCapacidad(), sens.getCoordX(), sens.getCoordY() );
		transitActual = (int) this.getCapacidad();
		connexionsOcupades = 0;
		c = 'n';
	}
	
	public Sensor(Sensor sens) {
		super( (int) sens.getCapacidad(), sens.getCoordX(), sens.getCoordY() );
		connexionsOcupades = sens.connexionsOcupades;
		transitActual = sens.transitActual;
		ConnexioSortint = sens.ConnexioSortint;
		c = sens.c;
	}
	
	public int getConnexionsOcupades() {
		return connexionsOcupades;
	}

	public void setConnexionsOcupades(int connexionsOcupades) {
		this.connexionsOcupades = connexionsOcupades;
	}

	public int getTransitActual() {
		return transitActual;
	}

	public void setTransitActual(int transitActual) {
		this.transitActual = transitActual;
	}

	public int getConnexioSortint() {
		return ConnexioSortint;
	}

	public void setConnexioSortint(int connexioSortint) {
		ConnexioSortint = connexioSortint;
	}

	public char getC() {
		return c;
	}

	public void setC(char c) {
		this.c = c;
	}

	@Override
	public int compareTo(Sensor o) {
		return (int) (o.getCapacidad() - getCapacidad());
	}

	@Override
	public boolean estaConnectat() {
		return c == 's' || c == 'c';
	}

	@Override
	public boolean pucConnectarme() {
		return connexionsOcupades+1 <= CONNEXIONS_MAXIMES;
	}

	@Override
	public void incrementaConnexionsOcupades() {
		++connexionsOcupades;
	}

	@Override
	public boolean superaTransitMaxim(int volum) {
		return  transitActual + volum > this.getCapacidad()*3;
	}

	@Override
	public void incrementaTransitActual(int volum) {
		transitActual += volum;
	}
	
	@Override
	public void decrementaConnexionsOcupades() {
		--connexionsOcupades;
	}
	
	@Override
	public void decrementaTransitActual(int volum) {
		transitActual -= volum;
	}
}
